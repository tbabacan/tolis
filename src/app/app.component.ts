import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import * as data from '../assets/data.json';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tolis';

  public _data: MessagesJson[];
  constructor() {
    this._data = data.default.conversation;
    this._data.reverse();
  }

  ngOnInit() {
  }
}

export class MessagesJson {
  sender: string;
  text: string;
  created_at: string;

  constructor(json?: any) {
    this.sender = json.sender;
    this.text = json.text;
    this.created_at = new Date(json.created_at).toLocaleDateString();
  }
}
